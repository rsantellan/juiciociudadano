<?php

namespace Maith\JuicioCiudadanoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Maith\JuicioCiudadanoBundle\Entity\MaithArticle;
use Maith\JuicioCiudadanoBundle\Form\MaithTextArticleType;

/**
 * MaithArticle controller.
 *
 */
class TextArticleController extends Controller
{

    /**
     * Lists all MaithArticle entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQuery("select c from MaithJuicioCiudadanoBundle:MaithArticle c where c.category = :category order by c.position asc")
                          ->setParameters(array(
                              'category' => 2
                          ))->getResult();
        
        //$em->getRepository('MaithJuicioCiudadanoBundle:MaithArticle')->findBy(array('category' => 2));

        return $this->render('MaithJuicioCiudadanoBundle:TextArticle:index.html.twig', array(
            'entities' => $entities,
            'activemenu' => 'textarticle',
        ));
    }
    /**
     * Creates a new MaithArticle entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new MaithArticle();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $category = $em->getRepository('MaithJuicioCiudadanoBundle:MaithArticleCategory')->find(2);
            $entity->setCategory($category);
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notif-success', 'Novedad creada con exito');
            return $this->redirect($this->generateUrl('admin_text_article_edit', array('id' => $entity->getId())));
        }
        else
        {
            $this->get('session')->getFlashBag()->add('notif-error', 'A ocurrido un error con el formulario. Revisa los campos.');
        }

        return $this->render('MaithJuicioCiudadanoBundle:TextArticle:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'activemenu' => 'textarticle',
        ));
    }

    /**
     * Creates a form to create a MaithArticle entity.
     *
     * @param MaithArticle $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MaithArticle $entity)
    {
        $form = $this->createForm(new MaithTextArticleType(), $entity, array(
            'action' => $this->generateUrl('admin_text_article_create'),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Displays a form to create a new MaithArticle entity.
     *
     */
    public function newAction()
    {
        $entity = new MaithArticle();
        $form   = $this->createCreateForm($entity);

        return $this->render('MaithJuicioCiudadanoBundle:TextArticle:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'activemenu' => 'textarticle',
        ));
    }

    /**
     * Finds and displays a MaithArticle entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MaithJuicioCiudadanoBundle:MaithArticle')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MaithArticle entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MaithJuicioCiudadanoBundle:TextArticle:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'activemenu' => 'textarticle',
        ));
    }

    /**
     * Displays a form to edit an existing MaithArticle entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MaithJuicioCiudadanoBundle:MaithArticle')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MaithArticle entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MaithJuicioCiudadanoBundle:TextArticle:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'activemenu' => 'textarticle',
        ));
    }

    /**
    * Creates a form to edit a MaithArticle entity.
    *
    * @param MaithArticle $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(MaithArticle $entity)
    {
        $form = $this->createForm(new MaithTextArticleType(), $entity, array(
            'action' => $this->generateUrl('admin_text_article_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing MaithArticle entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MaithJuicioCiudadanoBundle:MaithArticle')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MaithArticle entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('notif-success', 'Novedad actualizada con exito');
            return $this->redirect($this->generateUrl('admin_text_article_edit', array('id' => $id)));
        }  
        else 
        {
            $this->get('session')->getFlashBag()->add('notif-error', 'A ocurrido un error con el formulario. Revisa los campos.');
        }

        return $this->render('MaithJuicioCiudadanoBundle:TextArticle:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'activemenu' => 'textarticle',
        ));
    }
    /**
     * Deletes a MaithArticle entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MaithJuicioCiudadanoBundle:MaithArticle')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MaithArticle entity.');
            }
            $this->get('session')->getFlashBag()->add('notif-success', 'Novedad eliminada con exito');
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_text_article'));
    }

    /**
     * Creates a form to delete a MaithArticle entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_text_article_delete', array('id' => $id)))
            ->setMethod('DELETE')
            //->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
