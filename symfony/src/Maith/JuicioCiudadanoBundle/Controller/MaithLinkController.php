<?php

namespace Maith\JuicioCiudadanoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Maith\JuicioCiudadanoBundle\Entity\MaithLink;
use Maith\JuicioCiudadanoBundle\Form\MaithLinkType;

/**
 * MaithLink controller.
 *
 */
class MaithLinkController extends Controller
{

    /**
     * Lists all MaithLink entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MaithJuicioCiudadanoBundle:MaithLink')->findAll();

        return $this->render('MaithJuicioCiudadanoBundle:MaithLink:index.html.twig', array(
            'entities' => $entities,
            'activemenu' => 'links',
        ));
    }
    /**
     * Creates a new MaithLink entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new MaithLink();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notif-success', 'Link creado con exito');
            return $this->redirect($this->generateUrl('admin_links_edit', array('id' => $entity->getId())));
        }
        else 
        {
            $this->get('session')->getFlashBag()->add('notif-error', 'A ocurrido un error con el formulario. Revisa los campos.');
        }
        
        return $this->render('MaithJuicioCiudadanoBundle:MaithLink:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'activemenu' => 'links',
        ));
    }

    /**
     * Creates a form to create a MaithLink entity.
     *
     * @param MaithLink $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MaithLink $entity)
    {
        $form = $this->createForm(new MaithLinkType(), $entity, array(
            'action' => $this->generateUrl('admin_links_create'),
            'method' => 'POST',
        ));

        //$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new MaithLink entity.
     *
     */
    public function newAction()
    {
        $entity = new MaithLink();
        $form   = $this->createCreateForm($entity);

        return $this->render('MaithJuicioCiudadanoBundle:MaithLink:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'activemenu' => 'links',
        ));
    }

    /**
     * Finds and displays a MaithLink entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MaithJuicioCiudadanoBundle:MaithLink')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MaithLink entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MaithJuicioCiudadanoBundle:MaithLink:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'activemenu' => 'links',
        ));
    }

    /**
     * Displays a form to edit an existing MaithLink entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MaithJuicioCiudadanoBundle:MaithLink')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MaithLink entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MaithJuicioCiudadanoBundle:MaithLink:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'activemenu' => 'links',
        ));
    }

    /**
    * Creates a form to edit a MaithLink entity.
    *
    * @param MaithLink $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(MaithLink $entity)
    {
        $form = $this->createForm(new MaithLinkType(), $entity, array(
            'action' => $this->generateUrl('admin_links_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing MaithLink entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MaithJuicioCiudadanoBundle:MaithLink')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MaithLink entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('notif-success', 'Link actualizado con exito');
            return $this->redirect($this->generateUrl('admin_links_edit', array('id' => $id)));
        }
        else 
        {
            $this->get('session')->getFlashBag()->add('notif-error', 'A ocurrido un error con el formulario. Revisa los campos.');
        }
        return $this->render('MaithJuicioCiudadanoBundle:MaithLink:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'activemenu' => 'links',
        ));
    }
    /**
     * Deletes a MaithLink entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MaithJuicioCiudadanoBundle:MaithLink')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MaithLink entity.');
            }
            $this->get('session')->getFlashBag()->add('notif-success', 'Link eliminado con exito');
            $em->remove($entity);
            $em->flush();
        }
        else 
        {
            $this->get('session')->getFlashBag()->add('notif-error', 'A ocurrido un error. Intente nuevamente.');
        }
        return $this->redirect($this->generateUrl('admin_links'));
    }

    /**
     * Creates a form to delete a MaithLink entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_links_delete', array('id' => $id)))
            ->setMethod('DELETE')
            //->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
