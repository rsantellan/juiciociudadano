<?php

namespace Maith\JuicioCiudadanoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Maith\JuicioCiudadanoBundle\Entity\JuicioResultado;
use Maith\JuicioCiudadanoBundle\Form\JuicioResultadoType;

/**
 * JuicioResultado controller.
 *
 */
class JuicioResultadoController extends Controller
{

    /**
     * Lists all JuicioResultado entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MaithJuicioCiudadanoBundle:JuicioResultado')->findAll();

        return $this->render('MaithJuicioCiudadanoBundle:JuicioResultado:index.html.twig', array(
            'entities' => $entities,
            'activemenu' => 'resultados',
        ));
    }
    /**
     * Creates a new JuicioResultado entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new JuicioResultado();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_resultados_edit', array('id' => $entity->getId())));
        }

        return $this->render('MaithJuicioCiudadanoBundle:JuicioResultado:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'activemenu' => 'resultados',
        ));
    }

    /**
     * Creates a form to create a JuicioResultado entity.
     *
     * @param JuicioResultado $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(JuicioResultado $entity)
    {
        $form = $this->createForm(new JuicioResultadoType(), $entity, array(
            'action' => $this->generateUrl('admin_resultados_create'),
            'method' => 'POST',
        ));

        //$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new JuicioResultado entity.
     *
     */
    public function newAction()
    {
        $entity = new JuicioResultado();
        $form   = $this->createCreateForm($entity);

        return $this->render('MaithJuicioCiudadanoBundle:JuicioResultado:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'activemenu' => 'resultados',
        ));
    }

    /**
     * Finds and displays a JuicioResultado entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MaithJuicioCiudadanoBundle:JuicioResultado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find JuicioResultado entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MaithJuicioCiudadanoBundle:JuicioResultado:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'activemenu' => 'resultados',
        ));
    }

    /**
     * Displays a form to edit an existing JuicioResultado entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MaithJuicioCiudadanoBundle:JuicioResultado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find JuicioResultado entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MaithJuicioCiudadanoBundle:JuicioResultado:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'activemenu' => 'resultados',
        ));
    }

    /**
    * Creates a form to edit a JuicioResultado entity.
    *
    * @param JuicioResultado $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(JuicioResultado $entity)
    {
        $form = $this->createForm(new JuicioResultadoType(), $entity, array(
            'action' => $this->generateUrl('admin_resultados_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing JuicioResultado entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MaithJuicioCiudadanoBundle:JuicioResultado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find JuicioResultado entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_resultados_edit', array('id' => $id)));
        }

        return $this->render('MaithJuicioCiudadanoBundle:JuicioResultado:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'activemenu' => 'resultados',
        ));
    }
    /**
     * Deletes a JuicioResultado entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MaithJuicioCiudadanoBundle:JuicioResultado')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find JuicioResultado entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_resultados'));
    }

    /**
     * Creates a form to delete a JuicioResultado entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_resultados_delete', array('id' => $id)))
            ->setMethod('DELETE')
            //->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
