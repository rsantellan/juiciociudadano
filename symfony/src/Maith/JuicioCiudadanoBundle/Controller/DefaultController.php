<?php

namespace Maith\JuicioCiudadanoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Maith\JuicioCiudadanoBundle\Entity\MaithMultimedia;
use Maith\JuicioCiudadanoBundle\Entity\JuicioPrensa;
use Maith\JuicioCiudadanoBundle\Entity\JuicioAgenda;
use Maith\JuicioCiudadanoBundle\Entity\JuicioResultado;
use Maith\JuicioCiudadanoBundle\Form\JuicioAgendaType;
use Maith\JuicioCiudadanoBundle\Form\ContactType;

class DefaultController extends Controller
{
  
    public function textosCategoriesAction()
    {
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery("select c from MaithJuicioCiudadanoBundle:MaithTextArticleCategory c order by c.id asc");
      $categories = $query->useResultCache(true, 360)->getResult();
      $response = $this->render('MaithJuicioCiudadanoBundle:Default:textosCategorias.html.twig', array(
            'categories' => $categories
          ));
        
        $response->setPublic();
        $response->setSharedMaxAge("3600");
        return $response;
    }
  
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $queryMultimedia = $em->createQuery("select c from MaithJuicioCiudadanoBundle:MaithMultimedia c order by c.position asc")
                            ->setFirstResult(0)
                            ->setMaxResults(1);
        $multimedia = $queryMultimedia->useResultCache(true, 360)->getOneOrNullResult();;
        
        $mediaGallery = $this->get('media_gallery_manager');
        $files = $mediaGallery->getGalleryFiles('home');
        
        $response = $this->render('MaithJuicioCiudadanoBundle:Default:index.html.twig', array(
            'multimedia' => $multimedia,
            'files' => $files,
            'indexForm' => true,
          ));
        
        $response->setPublic();
        $response->setSharedMaxAge("3600");
        return $response;
    }
    
    public function downloadOriginalFileAction($id)
    {
      $em = $this->getDoctrine()->getManager();
      $file = $em->getRepository("MaithCommonAdminBundle:mFile")->find($id);
      $content = file_get_contents($file->getFullPath());
      $response = new Response();

      /* Figure out the MIME type (if not specified) */
      $known_mime_types = array(
          "pdf" => "application/pdf",
          "txt" => "text/plain",
          "html" => "text/html",
          "htm" => "text/html",
          "exe" => "application/octet-stream",
          "zip" => "application/zip",
          "doc" => "application/msword",
          "xls" => "application/vnd.ms-excel",
          "ppt" => "application/vnd.ms-powerpoint",
          "gif" => "image/gif",
          "png" => "image/png",
          "jpeg" => "image/jpeg",
          "jpg" => "image/jpg",
          "php" => "text/plain"
      );
      $mime_type = $file->getType();
      $file_extension = strtolower(substr(strrchr($file->getName(), "."), 1));
      if(!in_array($file->getType(), $known_mime_types))
      {
        
        if (array_key_exists($file_extension, $known_mime_types)) {
          $mime_type = $known_mime_types[$file_extension];
        }
      }
      $name = $file->getName();
      if($file->getShowName() !== '')
      {
        $name = $file->getShowName().'.'.$file_extension;
      }
      $response->headers->set('Content-Type', $mime_type);
      $response->headers->set('Content-Disposition', 'attachment;filename="'.$name);

      $response->setContent($content);
      return $response;
    }
    
    public function contactosAction()
    {
        return $this->render('MaithJuicioCiudadanoBundle:Default:contactos.html.twig', array(
            'activemenu' => 'contactos',
            'indexForm' => false,
        ));
    }
    
    public function antecedentesAction()
    {
        return $this->render('MaithJuicioCiudadanoBundle:Default:antecedentes.html.twig', array(
            'activemenu' => 'antecedentes',
            'indexForm' => false,
        ));
    }
    
    public function quienessomosAction()
    {
        return $this->render('MaithJuicioCiudadanoBundle:Default:quienessomos.html.twig', array(
            'activemenu' => 'quienessomos',
            'indexForm' => false,
        ));
    }
    
    public function miradanacionallocalAction()
    {
        return $this->render('MaithJuicioCiudadanoBundle:Default:miradanacionallocal.html.twig', array(
            'activemenu' => 'miradanacionallocal',
            'indexForm' => false,
        ));
    }
    
    public function financiadoresAction()
    {
        return $this->render('MaithJuicioCiudadanoBundle:Default:financiadores.html.twig', array(
            'activemenu' => 'financiadores',
            'indexForm' => false,
        ));
    }
    
    public function deliberacionAction()
    {
        return $this->render('MaithJuicioCiudadanoBundle:Default:deliberacion.html.twig', array(
            'activemenu' => 'deliberacion',
            'indexForm' => false,
        ));
    }
    
    public function metodologiaDelphiAction()
    {
        return $this->render('MaithJuicioCiudadanoBundle:Default:metodologiadelphi.html.twig', array(
            'activemenu' => 'delphi',
            'indexForm' => false,
        ));
    }
    
    public function metodologiaFocalesAction()
    {
        return $this->render('MaithJuicioCiudadanoBundle:Default:metodologiafocales.html.twig', array(
            'activemenu' => 'focales',
            'indexForm' => false,
        ));
    }
    
    public function metodologiaDialogosAction()
    {
        return $this->render('MaithJuicioCiudadanoBundle:Default:metodologiadialogos.html.twig', array(
            'activemenu' => 'dialogos',
            'indexForm' => false,
        ));
    }
    
    public function metodologiaAction()
    {
        return $this->render('MaithJuicioCiudadanoBundle:Default:metodologia.html.twig', array(
            'activemenu' => 'metodologia',
            'indexForm' => false,
        ));
    }
    
    public function agendaAction(Request $request)
    {
        $entity = new JuicioAgenda();
        $form = $this->createForm(new JuicioAgendaType(), $entity);
        if ($request->isMethod('POST')) {
          $form->bind($request);
          if ($form->isValid()) {
              $em = $this->getDoctrine()->getManager();
              $em->persist($entity);
              $em->flush();
             $thisUrl = $this->generateUrl('agenda');
             $mailer = $this->get('mailer');
             $message = $mailer->createMessage()
                  ->setSubject(sprintf('[Que pasa en Uruguay][%s] Contacto desde sitio web', 'Agenda'))
                  //->setFrom(array('test@test.com' => 'Juicio ciudadano'))
                  ->setReplyTo($form->get('email')->getData())
                  ->setTo('tusaportes@quedesarrollo.uy')
                  ->setBody(
                      $this->renderView(
                          'MaithJuicioCiudadanoBundle:Default:agendaEmail.html.twig',
                          array(
                            'agenda' => $entity,
                          )
                      )
                  );
              $mailer->send($message);

              $request->getSession()->getFlashBag()->add('success', 'Se a enviado tu consulta con exito. Te contestaremos a la brevedad');
              return $this->redirect($thisUrl);   
          }
          
        }
        return $this->render('MaithJuicioCiudadanoBundle:Default:agenda.html.twig', array(
            'activemenu' => 'agenda',
            'form' => $form->createView(),
            'indexForm' => false,
        ));
    }
    
    public function novedadesAction($page = 0)
    {
      $paginationResults = 12;
      $startPagination = $paginationResults * $page;
      $em = $this->getDoctrine()->getManager();
      $queryArticles = $em->createQuery("select c from MaithJuicioCiudadanoBundle:MaithArticle c where c.category = :category order by c.position asc")
                          ->setParameters(array(
                              'category' => 1
                          ))
                          ->setFirstResult($startPagination)
                          ->setMaxResults($paginationResults);
      $articles = $queryArticles->useResultCache(true, 360)->getResult();
      return $this->render('MaithJuicioCiudadanoBundle:Default:novedades.html.twig', array(
            'articles' => $articles,
            'activemenu' => 'novedades',
            'page' => $page,
            'rowperpage' => $paginationResults,
            'indexForm' => false,
          ));
    }
    
    public function novedadAction($id, $slug)
    {
      $em = $this->getDoctrine()->getManager();
      $article = $em->getRepository('MaithJuicioCiudadanoBundle:MaithArticle')->find($id);
      if (!$article) {
          throw $this->createNotFoundException('La novedad no existe');
      }
      $mainAlbum = $em->getRepository("MaithCommonAdminBundle:mAlbum")->findOneBy(array('object_id' => $article->getId(), 'object_class' => $article->getFullClassName(), 'name' => 'principal'));
      $response = $this->render('MaithJuicioCiudadanoBundle:Default:novedad.html.twig', array(
            'article' => $article,
            'album' => $mainAlbum,
            'activemenu' => 'novedades',
            'indexForm' => false,
          ));
      
      $response->setPublic();
      $response->setSharedMaxAge("3600");
      return $response;
    }
    
    public function textosAction($page = 0)
    {
      $paginationResults = 12;
      $startPagination = $paginationResults * $page;
      $em = $this->getDoctrine()->getManager();
      $queryArticles = $em->createQuery("select c from MaithJuicioCiudadanoBundle:MaithArticle c where c.category = :category order by c.position asc")
                          ->setParameters(array(
                              'category' => 2
                          ))
                          ->setFirstResult($startPagination)
                          ->setMaxResults($paginationResults);
      $articles = $queryArticles->useResultCache(true, 360)->getResult();
      return $this->render('MaithJuicioCiudadanoBundle:Default:textos.html.twig', array(
            'articles' => $articles,
            'activemenu' => 'textos',
            'page' => $page,
            'rowperpage' => $paginationResults,
            'indexForm' => false,
          ));
    }
    
    public function textosCategoriaAction($id, $slug, $page = 0)
    {
      $paginationResults = 12;
      $startPagination = $paginationResults * $page;
      $em = $this->getDoctrine()->getManager();
      $category = $em->getRepository('MaithJuicioCiudadanoBundle:MaithTextArticleCategory')->find($id);
      if (!$category) {
          throw $this->createNotFoundException('La categoria de textos no existe');
      }
      $queryArticles = $em->createQuery("select c from MaithJuicioCiudadanoBundle:MaithArticle c where c.category = :category and c.textCategory = :textCategory order by c.position asc")
                          ->setParameters(array(
                              'category' => 2,
                              'textCategory' => $id,
                          ))
                          ->setFirstResult($startPagination)
                          ->setMaxResults($paginationResults);
      $articles = $queryArticles->useResultCache(true, 360)->getResult();
      return $this->render('MaithJuicioCiudadanoBundle:Default:textosCategory.html.twig', array(
            'articles' => $articles,
            'category' => $category,
            'activemenu' => 'textos',
            'page' => $page,
            'rowperpage' => $paginationResults,
            'indexForm' => false,
          ));
    }
    
    public function textoAction($id, $slug)
    {
      $em = $this->getDoctrine()->getManager();
      $article = $em->getRepository('MaithJuicioCiudadanoBundle:MaithArticle')->find($id);
      if (!$article) {
          throw $this->createNotFoundException('La novedad no existe');
      }
      $mainAlbum = $em->getRepository("MaithCommonAdminBundle:mAlbum")->findOneBy(array('object_id' => $article->getId(), 'object_class' => $article->getFullClassName(), 'name' => 'principal'));
      $response = $this->render('MaithJuicioCiudadanoBundle:Default:texto.html.twig', array(
            'article' => $article,
            'album' => $mainAlbum,
            'activemenu' => 'textos',
            'indexForm' => false,
          ));
      
      $response->setPublic();
      $response->setSharedMaxAge("3600");
      return $response;
    }
    
    public function linksAction($page = 0)
    {
      $paginationResults = 12;
      $startPagination = $paginationResults * $page;
      $em = $this->getDoctrine()->getManager();
      $queryArticles = $em->createQuery("select c from MaithJuicioCiudadanoBundle:MaithLink c order by c.position asc")
                          ->setFirstResult($startPagination)
                          ->setMaxResults($paginationResults);
      $articles = $queryArticles->useResultCache(true, 360)->getResult();
      return $this->render('MaithJuicioCiudadanoBundle:Default:links.html.twig', array(
            'links' => $articles,
            'activemenu' => 'links',
            'page' => $page,
            'rowperpage' => $paginationResults,
            'indexForm' => false,
          ));
    }
    
    public function multimediaAction($page = 0)
    {
      $paginationResults = 12;
      $startPagination = $paginationResults * $page;
      $em = $this->getDoctrine()->getManager();
      $queryArticles = $em->createQuery("select c from MaithJuicioCiudadanoBundle:MaithMultimedia c order by c.position asc")
                          ->setFirstResult($startPagination)
                          ->setMaxResults($paginationResults);
      $multimedia = $queryArticles->useResultCache(true, 360)->getResult();
      
      $auxMultimedia = new MaithMultimedia();
      $queryAlbumsAvatar = $em->createQuery('select a, f from MaithCommonAdminBundle:mAlbum a join a.files f where a.object_class = :object_class and a.name = :album order by f.orden')
              ->setParameters(array(
                  'object_class' => $auxMultimedia->getFullClassName(),
                  'album' => 'avatar',
              ));
      $queryAlbumsAvatarResult = $queryAlbumsAvatar->useResultCache(true, 360)->getResult();
      $avatarData = array();
      foreach($queryAlbumsAvatarResult as $album)
      {
        if(!isset($avatarData[$album->getObjectId()]))
        {
          $avatarData[$album->getObjectId()] = $album->getFiles()->first();
        }
      }
      $queryAlbumsDefault = $em->createQuery('select a, f from MaithCommonAdminBundle:mAlbum a join a.files f where a.object_class = :object_class and a.name = :album order by f.orden')
              ->setParameters(array(
                  'object_class' => $auxMultimedia->getFullClassName(),
                  'album' => 'gallery',
              ));
      $queryAlbumsDefaultResult = $queryAlbumsDefault->useResultCache(true, 360)->getResult();
      $galleryData = array();
      foreach($queryAlbumsDefaultResult as $album)
      {
        if(!isset($galleryData[$album->getObjectId()]))
        {
          $galleryData[$album->getObjectId()] = $album->getFiles();
        }
      }
      return $this->render('MaithJuicioCiudadanoBundle:Default:multimedia.html.twig', array(
            'multimedia' => $multimedia,
            'activemenu' => 'multimedia',
            'avatarData' => $avatarData,
            'galleryData' => $galleryData,
            'page' => $page,
            'rowperpage' => $paginationResults,
            'indexForm' => false,
          ));
    }
    
    public function salaprensaAction($page = 0)
    {
      $paginationResults = 12;
      $startPagination = $paginationResults * $page;
      $em = $this->getDoctrine()->getManager();
      $queryArticles = $em->createQuery("select c from MaithJuicioCiudadanoBundle:JuicioPrensa c order by c.position asc")
                          ->setFirstResult($startPagination)
                          ->setMaxResults($paginationResults);
      $articles = $queryArticles->useResultCache(true, 360)->getResult();
      $auxPrensa = new JuicioPrensa();
      $queryAlbumsAvatar = $em->createQuery('select a, f from MaithCommonAdminBundle:mAlbum a join a.files f where a.object_class = :object_class and a.name = :album order by f.orden')
              ->setParameters(array(
                  'object_class' => $auxPrensa->getFullClassName(),
                  'album' => 'avatar',
              ));
      $queryAlbumsAvatarResult = $queryAlbumsAvatar->useResultCache(true, 360)->getResult();
      $avatarData = array();
      foreach($queryAlbumsAvatarResult as $album)
      {
        if(!isset($avatarData[$album->getObjectId()]))
        {
          $avatarData[$album->getObjectId()] = $album->getFiles()->first();
        }
      }
      return $this->render('MaithJuicioCiudadanoBundle:Default:salaprensa.html.twig', array(
            'articles' => $articles,
            'activemenu' => 'prensa',
            'page' => $page,
            'rowperpage' => $paginationResults,
            'avatarData' => $avatarData,
            'indexForm' => false,
          ));
    }
    
    public function resultadosAction($page = 0)
    {
      $paginationResults = 12;
      $startPagination = $paginationResults * $page;
      $em = $this->getDoctrine()->getManager();
      $queryResultados = $em->createQuery("select c from MaithJuicioCiudadanoBundle:JuicioResultado c order by c.position asc")
                          ->setFirstResult($startPagination)
                          ->setMaxResults($paginationResults);
      $resultados = $queryResultados->useResultCache(true, 360)->getResult();
      
      return $this->render('MaithJuicioCiudadanoBundle:Default:resultados.html.twig', array(
            'resultados' => $resultados,
            'activemenu' => 'resultados',
            'page' => $page,
            'rowperpage' => $paginationResults,
            'indexForm' => false,
          ));
    }
    
    public function resultadoAction($id, $slug)
    {
      $em = $this->getDoctrine()->getManager();
      $resultado = $em->getRepository('MaithJuicioCiudadanoBundle:JuicioResultado')->find($id);
      if (!$resultado) {
          throw $this->createNotFoundException('El resultado no existe');
      }
      $mainAlbum = $em->getRepository("MaithCommonAdminBundle:mAlbum")->findOneBy(
              array(
                  'object_id' => $resultado->getId(), 
                  'object_class' => $resultado->getFullClassName(), 
                  'name' => 'gallery'
                  ));
      $response = $this->render('MaithJuicioCiudadanoBundle:Default:resultado.html.twig', array(
            'resultado' => $resultado,
            'album' => $mainAlbum,
            'activemenu' => 'resultados',
            'indexForm' => false,
          ));
      
      $response->setPublic();
      $response->setSharedMaxAge("3600");
      return $response;
    }
    
    public function prensaAction($id, $slug)
    {
      $em = $this->getDoctrine()->getManager();
      $article = $em->getRepository('MaithJuicioCiudadanoBundle:JuicioPrensa')->find($id);
      if (!$article) {
          throw $this->createNotFoundException('La novedad no existe');
      }
      $mainAlbum = $em->getRepository("MaithCommonAdminBundle:mAlbum")->findOneBy(array('object_id' => $article->getId(), 'object_class' => $article->getFullClassName(), 'name' => 'gallery'));
      $response = $this->render('MaithJuicioCiudadanoBundle:Default:prensaarticulo.html.twig', array(
            'article' => $article,
            'album' => $mainAlbum,
            'activemenu' => 'prensa',
            'indexForm' => false,
          ));
      
      $response->setPublic();
      $response->setSharedMaxAge("3600");
      return $response;
    }
    
    public function contactFormAction()
    {
      $form = $this->createForm(new ContactType());
      return $this->render('MaithJuicioCiudadanoBundle:Default:contacto.html.twig', array(
             'form' => $form->createView(),
          ));
    }
    
    public function contactInsideFormAction()
    {
      $form = $this->createForm(new ContactType());
      return $this->render('MaithJuicioCiudadanoBundle:Default:contactoInside.html.twig', array(
             'form' => $form->createView(),
          ));
    }
    
    public function searchFormAction()
    {
      $form = $this->createForm(new ContactType());
      return $this->render('MaithJuicioCiudadanoBundle:Default:searchForm.html.twig', array(
             'form' => $form->createView(),
          ));
    }
    
    public function sendContactAction(Request $request)
    {
      $form = $this->createForm(new ContactType());
      
      $data = array(
          'isvalid' => false,
          'message' => 'El formulario no pudo enviarse. Intente mas tarde'
      );
      if ($request->isMethod('POST')) {
          $form->bind($request);

          if ($form->isValid()) {
              $mailer = $this->get('mailer');
              $message = $mailer->createMessage()
                   ->setSubject(sprintf('[Que pasa en URUGUAY][%s] Contacto desde sitio web', 'Contacto'))
                   //->setFrom(array('test@test.com' => 'Juicio ciudadano'))
                   ->setReplyTo($form->get('email')->getData())
                   ->setTo('contacto@quedesarrollo.uy')
                   ->setBody(
                       $this->renderView(
                           'MaithJuicioCiudadanoBundle:Default:contactEmail.html.twig',
                           array(
                             'email' => $form->get('email')->getData(),
                             'name' => $form->get('name')->getData(),
                             'message' => $form->get('message')->getData(),
                           )
                       ), 'text/html'
                   );
               $mailer->send($message);
               $data['isvalid'] = true;
               $data['message'] = 'El mensaje fue enviado con exito.';
          }
          else
          {
            $errors = $this->getErrorMessages($form);
            
            $data['message'] = 'error en los campos: ';
            foreach($errors as $error)
            {
              $data['message'] .= $error[0]; 
            }
          }
      }
      $response = new JsonResponse();
      $response->setData($data);
      return $response;
    }
    
    
    private function getErrorMessages(\Symfony\Component\Form\Form $form) {
      $errors = array();

      foreach ($form->getErrors() as $key => $error) {
          if ($form->isRoot()) {
              $errors['#'][] = $error->getMessage();
          } else {
              $errors[] = $error->getMessage();
          }
      }

      foreach ($form->all() as $child) {
          if (!$child->isValid()) {
              $errors[$child->getName()] = $this->getErrorMessages($child);
          }
      }

      return $errors;
    }
    
    public function searchAction(Request $request)
    {
      $search = $request->get('s');
      $message = '';
      $textosNovedades = array();
      $links = array();
      $resultados = array();
      $error = true;
      if($search == ''){
        $message = 'La busqueda no puede ser vacia.';
      }else{
        if(strlen($search) < 3)
        {
          $message = 'Debe de ingresar al menos dos caracteres';
        }
        else
        {
          $error = false;
          $em = $this->getDoctrine()->getManager();
          
          $param = '%'.$search.'%';
          $queryTextoNovedad = $em->createQuery("select c from MaithJuicioCiudadanoBundle:MaithArticle c where c.title like :title or c.preface like :preface or c.body like :body order by c.category desc, c.position asc")
                          ->setParameters(array(
                              'title' => $param,
                              'preface' => $param,
                              'body' => $param,
                          ));
          $textosNovedades = $queryTextoNovedad->getResult();
          
          $queryLinks = $em->createQuery('select c from MaithJuicioCiudadanoBundle:MaithLink c where c.title like :title or c.description like :description or c.link like :link order by c.position asc')
                          ->setParameters(array(
                              'title' => $param,
                              'description' => $param,
                              'link' => $param,
                          ));
          $links = $queryLinks->getResult();
          
          $queryResultados = $em->createQuery('select c from MaithJuicioCiudadanoBundle:JuicioResultado c where c.title like :title or c.preface like :preface or c.body like :body order by c.position asc')
                        ->setParameters(array(
                              'title' => $param,
                              'preface' => $param,
                              'body' => $param,
                          ));
          $resultados = $queryResultados->getResult();
        }
      }
      $response = $this->render('MaithJuicioCiudadanoBundle:Default:search.html.twig', array(
            'textoNovedades' => $textosNovedades,
            'links' => $links,
            'resultados' => $resultados,
            'search' => $search,
            'message' => $message,
            'activemenu' => 'search',
            'error' => $error,
            'indexForm' => false,
          ));
      
      $response->setPublic();
      $response->setSharedMaxAge("3600");
      return $response;
      
    }

}
