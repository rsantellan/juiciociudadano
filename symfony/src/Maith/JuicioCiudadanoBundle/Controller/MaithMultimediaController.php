<?php

namespace Maith\JuicioCiudadanoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Maith\JuicioCiudadanoBundle\Entity\MaithMultimedia;
use Maith\JuicioCiudadanoBundle\Form\MaithMultimediaType;

/**
 * MaithMultimedia controller.
 *
 */
class MaithMultimediaController extends Controller
{

    /**
     * Lists all MaithMultimedia entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MaithJuicioCiudadanoBundle:MaithMultimedia')->findAll();

        return $this->render('MaithJuicioCiudadanoBundle:MaithMultimedia:index.html.twig', array(
            'entities' => $entities,
            'activemenu' => 'multimedia',
        ));
    }
    /**
     * Creates a new MaithMultimedia entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new MaithMultimedia();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notif-success', 'Multimedia creada con exito');
            return $this->redirect($this->generateUrl('admin_multimedia_edit', array('id' => $entity->getId())));
        }  
        else 
        {
            $this->get('session')->getFlashBag()->add('notif-error', 'A ocurrido un error con el formulario. Revisa los campos.');
        }

        return $this->render('MaithJuicioCiudadanoBundle:MaithMultimedia:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'activemenu' => 'multimedia',
        ));
    }

    /**
     * Creates a form to create a MaithMultimedia entity.
     *
     * @param MaithMultimedia $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MaithMultimedia $entity)
    {
        $form = $this->createForm(new MaithMultimediaType(), $entity, array(
            'action' => $this->generateUrl('admin_multimedia_create'),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Displays a form to create a new MaithMultimedia entity.
     *
     */
    public function newAction()
    {
        $entity = new MaithMultimedia();
        $form   = $this->createCreateForm($entity);

        return $this->render('MaithJuicioCiudadanoBundle:MaithMultimedia:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'activemenu' => 'multimedia',
        ));
    }

    /**
     * Finds and displays a MaithMultimedia entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MaithJuicioCiudadanoBundle:MaithMultimedia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MaithMultimedia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MaithJuicioCiudadanoBundle:MaithMultimedia:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'activemenu' => 'multimedia',
        ));
    }

    /**
     * Displays a form to edit an existing MaithMultimedia entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MaithJuicioCiudadanoBundle:MaithMultimedia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MaithMultimedia entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MaithJuicioCiudadanoBundle:MaithMultimedia:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'activemenu' => 'multimedia',
        ));
    }

    /**
    * Creates a form to edit a MaithMultimedia entity.
    *
    * @param MaithMultimedia $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(MaithMultimedia $entity)
    {
        $form = $this->createForm(new MaithMultimediaType(), $entity, array(
            'action' => $this->generateUrl('admin_multimedia_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }
    /**
     * Edits an existing MaithMultimedia entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MaithJuicioCiudadanoBundle:MaithMultimedia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MaithMultimedia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('notif-success', 'Multimedia actualizada con exito');
            return $this->redirect($this->generateUrl('admin_multimedia_edit', array('id' => $id)));
        }
        else 
        {
            $this->get('session')->getFlashBag()->add('notif-error', 'A ocurrido un error con el formulario. Revisa los campos.');
        }

        return $this->render('MaithJuicioCiudadanoBundle:MaithMultimedia:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'activemenu' => 'multimedia',
        ));
    }
    /**
     * Deletes a MaithMultimedia entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MaithJuicioCiudadanoBundle:MaithMultimedia')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MaithMultimedia entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_multimedia'));
    }

    /**
     * Creates a form to delete a MaithMultimedia entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_multimedia_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
