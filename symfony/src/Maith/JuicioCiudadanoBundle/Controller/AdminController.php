<?php

namespace Maith\JuicioCiudadanoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Maith\Common\AdminBundle\Model\BcuCotizadorData;

class AdminController extends Controller
{
    public function indexAction()
    {
      
        //$cotizador = new BcuCotizadorData();
        $data = array();//$cotizador->retrieveLastUsableBcuCotizacion();
        return $this->render('MaithJuicioCiudadanoBundle:Admin:index.html.twig', array(
                'cotizaciones' => $data,
            ));    
        
        
    }
    
    public function galleriesAction()
    {
      return $this->render('MaithJuicioCiudadanoBundle:Admin:gallerie.html.twig', array(
                
            )); 
    }

}
