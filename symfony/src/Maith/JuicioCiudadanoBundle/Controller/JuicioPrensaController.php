<?php

namespace Maith\JuicioCiudadanoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Maith\JuicioCiudadanoBundle\Entity\JuicioPrensa;
use Maith\JuicioCiudadanoBundle\Form\JuicioPrensaType;

/**
 * JuicioPrensa controller.
 *
 */
class JuicioPrensaController extends Controller
{

    /**
     * Lists all JuicioPrensa entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MaithJuicioCiudadanoBundle:JuicioPrensa')->findAll();

        return $this->render('MaithJuicioCiudadanoBundle:JuicioPrensa:index.html.twig', array(
            'entities' => $entities,
            'activemenu' => 'article',
        ));
    }
    /**
     * Creates a new JuicioPrensa entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new JuicioPrensa();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notif-success', 'Articulo de prensa creado con exito');
            return $this->redirect($this->generateUrl('admin_prensa_edit', array('id' => $entity->getId())));
        }
        else
        {
            $this->get('session')->getFlashBag()->add('notif-error', 'A ocurrido un error con el formulario. Revisa los campos.');
        }

        return $this->render('MaithJuicioCiudadanoBundle:JuicioPrensa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'activemenu' => 'article',
        ));
    }

    /**
     * Creates a form to create a JuicioPrensa entity.
     *
     * @param JuicioPrensa $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(JuicioPrensa $entity)
    {
        $form = $this->createForm(new JuicioPrensaType(), $entity, array(
            'action' => $this->generateUrl('admin_prensa_create'),
            'method' => 'POST',
        ));

        //$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new JuicioPrensa entity.
     *
     */
    public function newAction()
    {
        $entity = new JuicioPrensa();
        $form   = $this->createCreateForm($entity);

        return $this->render('MaithJuicioCiudadanoBundle:JuicioPrensa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'activemenu' => 'article',
        ));
    }

    /**
     * Finds and displays a JuicioPrensa entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MaithJuicioCiudadanoBundle:JuicioPrensa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find JuicioPrensa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MaithJuicioCiudadanoBundle:JuicioPrensa:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'activemenu' => 'article',
        ));
    }

    /**
     * Displays a form to edit an existing JuicioPrensa entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MaithJuicioCiudadanoBundle:JuicioPrensa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find JuicioPrensa entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MaithJuicioCiudadanoBundle:JuicioPrensa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'activemenu' => 'article',
        ));
    }

    /**
    * Creates a form to edit a JuicioPrensa entity.
    *
    * @param JuicioPrensa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(JuicioPrensa $entity)
    {
        $form = $this->createForm(new JuicioPrensaType(), $entity, array(
            'action' => $this->generateUrl('admin_prensa_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing JuicioPrensa entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MaithJuicioCiudadanoBundle:JuicioPrensa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find JuicioPrensa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('notif-success', 'Articulo de prensa actualizado con exito');
            return $this->redirect($this->generateUrl('admin_prensa_edit', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add('notif-error', 'A ocurrido un error con el formulario. Revisa los campos.');
        }

        return $this->render('MaithJuicioCiudadanoBundle:JuicioPrensa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'activemenu' => 'article',
        ));
    }
    /**
     * Deletes a JuicioPrensa entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MaithJuicioCiudadanoBundle:JuicioPrensa')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find JuicioPrensa entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notif-success', 'Articulo de prensa eliminado con exito');
        }

        return $this->redirect($this->generateUrl('admin_prensa'));
    }

    /**
     * Creates a form to delete a JuicioPrensa entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_prensa_delete', array('id' => $id)))
            ->setMethod('DELETE')
            //->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
