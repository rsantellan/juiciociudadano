<?php

namespace Maith\JuicioCiudadanoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class JuicioPrensaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('preface')
            ->add('body')
            ->add('position')
            //->add('created')
            //->add('updated')
            //->add('slug')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Maith\JuicioCiudadanoBundle\Entity\JuicioPrensa'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'maith_juiciociudadanobundle_juicioprensa';
    }
}
