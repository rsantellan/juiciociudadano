<?php

namespace Maith\JuicioCiudadanoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class JuicioAgendaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('apellido')
            ->add('email')
            ->add('edad')
            ->add('departamento')
            ->add('ciudad')
            ->add('mensaje')
            //->add('created')
            //->add('updated')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Maith\JuicioCiudadanoBundle\Entity\JuicioAgenda'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'maith_juiciociudadanobundle_juicioagenda';
    }
}
