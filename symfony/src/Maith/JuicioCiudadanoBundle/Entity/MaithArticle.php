<?php

namespace Maith\JuicioCiudadanoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * MaithArticle
 *
 * @ORM\Table(name="maith_article")
 * @ORM\Entity(repositoryClass="Gedmo\Sortable\Entity\Repository\SortableRepository")
 */
class MaithArticle
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="preface", type="string", length=255, nullable=true)
     */
    private $preface;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     */
    private $body;


    /**
     * @var integer
     *
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position = 0;

    /**
      * @var datetime $created
      *
      * @Gedmo\Timestampable(on="create")
      * @ORM\Column(type="datetime")
      */
     private $created;

    /**
     * @var datetime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;
    
    
    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(name="slug", type="string", length=255)
     */    
    private $slug;
    
    
  /**
   * @ORM\ManyToOne(targetEntity="MaithArticleCategory", inversedBy="articles")
   * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
   */
	private $category;

  /**
   * @ORM\ManyToOne(targetEntity="MaithTextArticleCategory", inversedBy="articles")
   * @ORM\JoinColumn(name="text_category_id", referencedColumnName="id", nullable=true)
   */
	private $textCategory;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return MaithArticle
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set preface
     *
     * @param string $preface
     * @return MaithArticle
     */
    public function setPreface($preface)
    {
        $this->preface = $preface;

        return $this;
    }

    /**
     * Get preface
     *
     * @return string 
     */
    public function getPreface()
    {
        return $this->preface;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return MaithArticle
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return MaithArticle
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return MaithArticle
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return MaithArticle
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return MaithArticle
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    public function getFullClassName()
    {
      return get_class($this);
    }
    
    public function retrieveAlbums()
    {
      return array('principal');
    }

    public function checkAlbumForOnlineVideo($name)
    {
      if($name == 'principal')
      {
        return true;
      }
      return false;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->category = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set category
     *
     * @param \Maith\JuicioCiudadanoBundle\Entity\MaithArticleCategory $category
     * @return MaithArticle
     */
    public function setCategory(\Maith\JuicioCiudadanoBundle\Entity\MaithArticleCategory $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Maith\JuicioCiudadanoBundle\Entity\MaithArticleCategory 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set textCategory
     *
     * @param \Maith\JuicioCiudadanoBundle\Entity\MaithTextArticleCategory $textCategory
     * @return MaithArticle
     */
    public function setTextCategory(\Maith\JuicioCiudadanoBundle\Entity\MaithTextArticleCategory $textCategory = null)
    {
        $this->textCategory = $textCategory;

        return $this;
    }

    /**
     * Get textCategory
     *
     * @return \Maith\JuicioCiudadanoBundle\Entity\MaithTextArticleCategory 
     */
    public function getTextCategory()
    {
        return $this->textCategory;
    }
}
