<?php

namespace Maith\JuicioCiudadanoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MaithArticleCategory
 *
 * @ORM\Table(name="maith_article_category")
 * @ORM\Entity
 */
class MaithArticleCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="MaithArticle", mappedBy="category")
     */
    private $articles;
    
    
    /**
     * Set id
     *
     * @return integer 
     */
    public function setId($id)
    {
        $this->id = $id;
        return  $this;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MaithArticleCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->articles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add articles
     *
     * @param \Maith\JuicioCiudadanoBundle\Entity\MaithArticle $articles
     * @return MaithArticleCategory
     */
    public function addArticle(\Maith\JuicioCiudadanoBundle\Entity\MaithArticle $articles)
    {
        $this->articles[] = $articles;

        return $this;
    }

    /**
     * Remove articles
     *
     * @param \Maith\JuicioCiudadanoBundle\Entity\MaithArticle $articles
     */
    public function removeArticle(\Maith\JuicioCiudadanoBundle\Entity\MaithArticle $articles)
    {
        $this->articles->removeElement($articles);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArticles()
    {
        return $this->articles;
    }
}
