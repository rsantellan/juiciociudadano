<?php

namespace Maith\JuicioCiudadanoBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Description of LoadUserFixture
 *
 * @author Rodrigo Santellan
 */
class LoadUserFixture extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface{
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    public function getOrder() {
        return 2;
    }

    public function load(ObjectManager $manager) {
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $user->setUsername('rsantellan@gmail.com');
        $user->setEmail('rsantellan@gmail.com');
        $user->setPlainPassword('1234');
        $user->setEnabled(true);
        $user->setSuperAdmin(true);
        $user->setFullName("Rodrigo Santellan");
        $manager->persist($user);
        
        $admin = $userManager->createUser();
        $admin->setUsername('admin@admin.com');
        $admin->setEmail('admin@admin.com');
        $admin->setPlainPassword('Administrator.1234');
        $admin->setEnabled(true);
        $admin->setSuperAdmin(true);
        $admin->setFullName("Administrador");
        $manager->persist($admin);
        
        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }
}


