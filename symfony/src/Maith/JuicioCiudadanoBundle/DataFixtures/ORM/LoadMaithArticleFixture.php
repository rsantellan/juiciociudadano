<?php

namespace Maith\JuicioCiudadanoBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Maith\JuicioCiudadanoBundle\Entity\MaithArticleCategory;
use Maith\JuicioCiudadanoBundle\Entity\MaithTextArticleCategory;
// use Maith\JuicioCiudadanoBundle\Entity\MaithArticle;
/**
 * Description of LoadUserFixture
 *
 * @author Rodrigo Santellan
 */
class LoadMaithArticleFixture extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface{
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    public function getOrder() {
        return 1;
    }

    public function load(ObjectManager $manager) {
      
        $novedades = new MaithArticleCategory();
        $novedades->setId(1);
        $novedades->setName('Novedades');
        $manager->persist($novedades);
        $metadata = $manager->getClassMetaData(get_class($novedades));
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

        $textos = new MaithArticleCategory();
        $textos->setId(2);
        $textos->setName('Textos');
        $manager->persist($textos);
        $metadataTextos = $manager->getClassMetaData(get_class($textos));
        $metadataTextos->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        
        
        $textoAntecedentes = new MaithTextArticleCategory();
        $textoAntecedentes->setName(ucwords(strtolower('ANTECEDENTES DE PARTICIPACIÓN PÚBLICA EN PROBLEMAS SOCIO-AMBIENTALES')));
        $textoAntecedentes->setId(1);
        $manager->persist($textoAntecedentes);
        $metadatatextoAntecedentes = $manager->getClassMetaData(get_class($textoAntecedentes));
        $metadatatextoAntecedentes->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        
        
        $textoDocumentos = new MaithTextArticleCategory();
        $textoDocumentos->setName(ucwords(strtolower('DOCUMENTOS SOBRE EL PROYECTO')));
        $textoDocumentos->setId(2);
        $manager->persist($textoDocumentos);
        $metadatatextoDocumentos = $manager->getClassMetaData(get_class($textoDocumentos));
        $metadatatextoDocumentos->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        
        /*
        $textoProblemas = new MaithTextArticleCategory();
        $textoProblemas->setName(ucwords(strtolower('PROBLEMAS SOCIO-AMBIENTALES')));
        $textoProblemas->setId(3);
        $manager->persist($textoProblemas);
        $metadatatextoProblemas = $manager->getClassMetaData(get_class($textoProblemas));
        $metadatatextoProblemas->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        */
        
        $manager->flush();
        
        
        
    }

    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }
}


