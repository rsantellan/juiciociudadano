<header class="wrapper light" style="width:100%">
	<div class="menu">
		<a href="/index.php"><img class="logo" src="img/logo.png" alt=""/></a>
<div class="menu_top">
   <ul>
      <li><a href="#" <?php if($pagina == 'contactos'):?> class="current"<?php endif;?>>Contactos</a></li>
      <li><a href="/prensa.php" <?php if($pagina == 'prensa'):?> class="current"<?php endif;?>>Sala de prensa</a></li>
      <li><a href="/novedades.php" <?php if($pagina == 'novedades'):?> class="current"<?php endif;?>>Novedades</a></li>
   </ul>
   <form>
      <div class="div_search">
         <input type="text" name="search"  placeholder="buscar" id="search"><input type="submit" class="input_submit" value="">
      </div>
   </form>
</div>
<div id='cssmenu'>
<ul>
   <li class='active has-sub'><a href="#" <?php if($categoria == 'proyecto'):?> class="current"<?php endif;?>><span>el proyecto</span></a>
      <ul>
         <li><a href="/antecedentes.php" <?php if($pagina == 'antecedentes'):?> class="current"<?php endif;?>><span>Antecedente</span></a></li>
         <li><a href="/nosotros.php" <?php if($pagina == 'nosotros'):?> class="current"<?php endif;?>><span>Quiénes somos</span></a></li>
         <li><a href="/mirada.php" <?php if($pagina == 'mirada'):?> class="current"<?php endif;?>><span>Mirada nacional y local</span></a></li>
         <li><a href="/financiadores.php" <?php if($pagina == 'financiadores'):?> class="current"<?php endif;?>><span>Financiadores</span></a></li>
         <li><a href="/resultados.php" <?php if($pagina == 'resultados'):?> class="current"<?php endif;?>><span>Resultados</span></a></li>
      </ul>
   </li>
   <li class='active has-sub'><a href="#" <?php if($categoria == 'enfoque'):?> class="current"<?php endif;?>><span>Enfoque</span></a>
      <ul>
         <li><a href="/deliberacion.php" <?php if($pagina == 'deliberacion'):?> class="current"<?php endif;?>><span>Deliberación y Desarrollo</span></a></li>
        <li class='has-sub'><a href="#" <?php if($subcategoria == 'metodologia'):?> class="current"<?php endif;?>><span>Metodología</span></a>
            <ul>
               <li><a href="/delphi.php" <?php if($pagina == 'delphi'):?> class="current"<?php endif;?>><span>Delphi</span></a></li>
               <li><a href="/focales.php" <?php if($pagina == 'focales'):?> class="current"<?php endif;?>><span>Grupos Focales</span></a></li>
               <li class='last'><a href="/dialogos_ciudadanos.php" <?php if($pagina == 'dialogos_ciudadanos'):?> class="current"<?php endif;?>><span>Diálogos Ciudadanos</span></a></li>
            </ul>
         </li>
      </ul>
   </li>
   <li class='active has-sub'><a href="#" <?php if($categoria == 'biblioteca'):?> class="current"<?php endif;?>><span>biblioteca</span></a>
      <ul>
         <li><a href="/textos.php" <?php if($pagina == 'textos'):?> class="current"<?php endif;?>><span>textos</span></a></li>
         <li><a href="/multimedia.php" <?php if($pagina == 'multimedia'):?> class="current"<?php endif;?>><span>multimedia</span></a></li>
         <li><a href="/links.php" <?php if($pagina == 'links'):?> class="current"<?php endif;?>><span>links de interés</span></a></li>
      </ul>
   </li>
   <li class='last'><a href="/agenda.php" <?php if($pagina == 'agenda'):?> class="current"<?php endif;?>><span>incid&iacute; en la agenda</span></a></li>
</ul>
</div>
	</div>
</header>

