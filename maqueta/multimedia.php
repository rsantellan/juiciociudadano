<!DOCTYPE html>
<?php $categoria  = 'biblioteca';?>
<?php $pagina  = 'multimedia';?>
<html lang="en">
<?php include('_head.php');?>
<body>
<?php include('_header.php');?>

	<section class="wrapper">
		<div class="bullet_vertical">
			<img src="/img/bullet_vertical.png">
		</div>
	</section>

	<section class="wrapper internas">
		<div class="internas_content">
			<img src="/img/biblioteca.png" alt="" class="animated wow fadeInDown"/>
		</div>	
		<div class="title animated wow fadeIn">
		<h1>multimedia</h1>
		</div>
	</section><!--  End participa  -->


	<section class="videos">
		<div class="wrapper">
			<div class="title animated wow fadeIn">
				<hr class="separator"/>
			</div>

			<ul class="clearfix">

				<li class="animated wow fadeInDown grid">
					<figure class="effect-kira">
						<img src="img/2.jpg" alt="img05"/>
						<figcaption>
							<h2>video <span>abc</span></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.<a href="http://www.youtube.com/embed/VOJyrQa_WR4?rel=0&amp;wmode=transparent" class="youtube multimedia_video" data-wow-duration="2s">ver video</a></p>							
						</figcaption>		
					</figure>
			</script>
				</li>
				<li class="animated wow fadeInDown grid" data-wow-delay=".2s">
					<figure class="effect-kira">
						<img src="img/4.jpg" alt="img05"/>
						<figcaption>
							<h2>video <span>klm</span></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.<a href="http://www.youtube.com/embed/VOJyrQa_WR4?rel=0&amp;wmode=transparent" class="youtube multimedia_video" class="multimedia_video">ver video</a></p>							
						</figcaption>		
					</figure>
				</li>

				<li class="animated wow fadeInDown grid" data-wow-delay=".2s">
					<figure class="effect-kira">
						<img src="img/7.jpg" alt="img05"/>
						<figcaption>
							<h2>video <span>pqr</span></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.<a href="http://www.youtube.com/embed/VOJyrQa_WR4?rel=0&amp;wmode=transparent" class="youtube multimedia_video" class="multimedia_video">ver video</a></p>							
						</figcaption>		
					</figure>
				</li>
				<li class="animated wow fadeInDown grid">
					<figure class="effect-kira">
						<img src="img/2.jpg" alt="img05"/>
						<figcaption>
							<h2>galer&iacute;a <span>abc</span></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.
                                                                <a href="img/2.jpg" rel="gal1" class="group2 multimedia_galeria" title="nombre foto trtrter">ver galer&iacute;a</a>
								<a href="img/15.jpg" class="group2 gallery_group" title="nombre foto sadsdsa"></a>
								<a href="img/18.jpg" class="group2 gallery_group" title="nombre foto ghghdfgfd"></a>
								<a href="img/28.jpg" class="group2 gallery_group" title="nombre foto kjjyukyu"></a>
							</p>							
						</figcaption>			
					</figure>
				</li>

				<li class="animated wow fadeInDown grid" data-wow-delay=".2s">
					<figure class="effect-kira">
						<img src="img/4.jpg" alt="img05"/>
						<figcaption>
							<h2>galer&iacute;a <span>klm</span></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.<a href="img/4.jpg" class="group3 multimedia_galeria" title="nombre foto trtrter">ver galer&iacute;a</a></p>							
								<a href="img/15.jpg" class="group3 gallery_group" title="nombre foto sadsdsa"></a>
								<a href="img/18.jpg" class="group3 gallery_group" title="nombre foto ghghdfgfd"></a>
						</figcaption>		
					</figure>
				</li>

				<li class="animated wow fadeInDown grid" data-wow-delay=".2s">
					<figure class="effect-kira">
						<img src="img/7.jpg" alt="img05"/>
						<figcaption>
							<h2>montevideo<span>2015</span></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.<a href="img/7.jpg" class="group4 multimedia_galeria" title="Montevideo 2015">ver imagen</a></p>							
						</figcaption>		
					</figure>
				</li>
			</ul>
		</div>
		<div class="title animated wow fadeIn">
			<hr class="separator" style="margin-top:20px;"/>
		</div>
		<script>
			$(document).ready(function(){
				//Examples of how to assign the Colorbox event to elements
				$(".group2").colorbox({rel:'group2', transition:"fade"});
				$(".group3").colorbox({rel:'group3', transition:"fade"});
				$(".group4").colorbox({rel:'group4', transition:"fade"});
				$(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});

				//Example of preserving a JavaScript event for inline calls.
				$("#click").click(function(){ 
					$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});
			});
		</script>		
	</section> <!-- videos End videos  -->




	<section class="contacto_internas">
		<div class="wrapper">
			<div class="contacto_internas_content">
		<div class="title animated wow fadeIn">
			<h2>contacto</h2>
		</div>
		<form>
			<input type="text" placeholder="NOMBRE*"><input type="text" placeholder="MAIL*">
			<textarea placeholder="MENSAJE*" rows="10"></textarea>
			<span><sub>*</sub>Campos obligatorios</span>
			<input type="submit" class="input_submit" value="enviar" style="margin-left:495px;">
		</form>
		<div class="clear"></div>
	</section><!--  End contacto  -->


<?php include('_footer.php');?>
    <script src='../ga.js'></script>
</body>
</html>
