<!DOCTYPE html>
<html lang="en">
<?php include('_head.php');?>
<body>
<?php include('_header.php');?>


	<section class="wrapper">
		<div class="bullet_vertical">
			<img src="/img/bullet_vertical.png">
		</div>
	</section>

	<section class="wrapper internas">
		<div class="internas_content">
			<img src="/img/busqueda.png" alt="" class="animated wow fadeInDown"/>
		</div>	
		<div class="title animated wow fadeIn">
		<h1>resultados de búsqueda</h1>
		</div>
	</section><!--  End participa  -->

	<section class="wrapper">
		<div class="bullet_vertical bullet_vertical_down">
			<img src="/img/bullet_vertical.png">
		</div>
	</section>

	<section class="internas wrapper" id="section-about">
		<div class="title animated wow fadeIn">
			<h4>Grupos focales febrero 2015</h4>
			<hr class="separator"/>
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel orci consequat, fermentum mauris luctus, semper mauris. Vivamus quis elit orci. Ut massa eros, malesuada at tellus eu, tincidunt tincidunt urna...</p>
		<a href="#" class="busqueda">ver más</a>
		<div class="title animated wow fadeIn">
			<hr class="separator" style="margin-top:20px;"/>
		</div>
	</section><!--  End resultados  -->

	<section class="internas wrapper" id="section-about">
		<div class="title animated wow fadeIn">
			<h4>Grupos focales diciembre 2014</h4>
			<hr class="separator"/>
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel orci consequat, fermentum mauris luctus, semper mauris. Vivamus quis elit orci. Ut massa eros, malesuada at tellus eu, tincidunt tincidunt urna...</p>
		<a href="#" class="busqueda">ver más</a>
		<div class="title animated wow fadeIn">
			<hr class="separator" style="margin-top:20px;"/>
		</div>
	</section><!--  End resultados  -->	

	<section class="internas wrapper" id="section-about">
		<div class="paginacion"><a href=""> < Anterior</a> | <a href="">Siguiente > </a> </div>
	</section>

	<section class="contacto_internas">
		<div class="wrapper">
			<div class="contacto_internas_content">
		<div class="title animated wow fadeIn">
			<h2>contacto</h2>
		</div>
		<form>
			<input type="text" placeholder="NOMBRE*"><input type="text" placeholder="MAIL*">
			<textarea placeholder="MENSAJE*" rows="10"></textarea>
			<span><sub>*</sub>Campos obligatorios</span>
			<input type="submit" class="input_submit" value="enviar" style="margin-left:495px;">
		</form>
		<div class="clear"></div>
	</section><!--  End contacto  -->


<?php include('_footer.php');?>
    <script src='../ga.js'></script>
</body>
</html>