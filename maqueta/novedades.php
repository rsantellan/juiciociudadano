<!DOCTYPE html>
<?php $pagina  = 'novedades';?>
<html lang="en">
<?php include('_head.php');?>
<body>
<?php include('_header.php');?>


	<section class="wrapper">
		<div class="bullet_vertical">
			<img src="/img/bullet_vertical.png">
		</div>
	</section>

	<section class="wrapper internas">
		<div class="internas_content">
			<img src="/img/novedades_int.png" alt="" class="animated wow fadeInDown"/>
		</div>	
		<div class="title animated wow fadeIn">
		<h1>&uacute;ltimas noticias</h1>
		</div>
	</section><!--  End participa  -->

	<div class="title animated wow fadeIn">
		<hr class="separator"/>
	</div>
	<section class="bloque_top internas wrapper" style="margin-top:0;">
		<ul class="clearfix">
			<li class="animated wow fadeInDown">
				<span class="separator"></span>
				<h2>Novedad 1</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="/novedad1.php">ver más</a>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".2s">
				<span class="separator"></span>
				<h2>Novedad 2</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="#">ver más</a>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".4s">
				<span class="separator"></span>
				<h2>Novedad 3</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="#">ver más</a>			
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".6s">
				<span class="separator"></span>
				<h2>Novedad 4</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="#">ver más</a>			
			</li>
			<li class="animated wow fadeInDown">
				<span class="separator"></span>
				<h2>Novedad 5</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="#">ver más</a>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".2s">
				<span class="separator"></span>
				<h2>Novedad 6</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="#">ver más</a>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".4s">
				<span class="separator"></span>
				<h2>Novedad 7</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="#">ver más</a>			
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".6s">
				<span class="separator"></span>
				<h2>Novedad 8</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="#">ver más</a>			
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".4s">
				<span class="separator"></span>
				<h2>Novedad 9</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="#">ver más</a>			
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".6s">
				<span class="separator"></span>
				<h2>Novedad 10</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="#">ver más</a>			
			</li>
		</ul>

		<div class="title animated wow fadeIn">
			<hr class="separator" style="margin-top:20px;"/>
		</div>
	</section><!--  End text  -->




	<section class="contacto_internas">
		<div class="wrapper">
			<div class="contacto_internas_content">
		<div class="title animated wow fadeIn">
			<h2>contacto</h2>
		</div>
		<form>
			<input type="text" placeholder="NOMBRE*"><input type="text" placeholder="MAIL*">
			<textarea placeholder="MENSAJE*" rows="10"></textarea>
			<span><sub>*</sub>Campos obligatorios</span>
			<input type="submit" class="input_submit" value="enviar" style="margin-left:495px;">
		</form>
		<div class="clear"></div>
	</section><!--  End contacto  -->


<?php include('_footer.php');?>
    <script src='../ga.js'></script>
</body>
</html>