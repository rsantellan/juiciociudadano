<!DOCTYPE html>
<html lang="en">
<?php include('_head.php');?>
<body>
<?php include('_header.php');?>
	<section class="billboard light">
		<div class="caption light animated wow fadeInDown clearfix">
      <section class="slider">
        <div class="flexslider">
          <ul class="slides">
            <li>
  	    	    <img src="/img/billboard2.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="/img/billboard2.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="/img/billboard2.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="/img/billboard2.jpg" />
  	    		</li>
          </ul>
        </div>
      </section>
		</div>
		<div class="shadow"></div>
	</section><!--  End slider  -->


	<section class="bloque_top wrapper">
		<ul class="clearfix">
			<li class="animated wow fadeInDown">
				<span class="separator"></span>
				<h2>Desarrollo y Deliberaci&oacute;n</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="#">ver más</a>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".2s">
				<span class="separator"></span>
				<h2>M&eacute;todo DELPHI</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="#">ver más</a>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".4s">
				<span class="separator"></span>
				<h2>Di&aacute;logos Cuidadanos</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="#">ver más</a>			
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".6s">
				<span class="separator"></span>
				<h2>Grupos Focales</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="#">ver más</a>			
			</li>
		</ul>
	</section><!--  End bloque_top  -->

	<section class="wrapper">
		<div class="bullet_vertical">
			<img src="/img/bullet_vertical.png">
		</div>
	</section>

	<section class="wrapper participa">
		<div class="participa_content">
			<img src="/img/novedades.png" alt="" class="animated wow fadeInDown"/>
		
		<ul class="clearfix">
			<li class="animated wow fadeInDown">
				<h2>Nuevo Informe</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="#">ver más</a>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".2s">
				<h2>Resultados Grupos focales</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="#">ver más</a>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".4s">
				<h2>Documental - ¿Megaminer&iacute;a?</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices ipsum, in blandit urna condimentum sit amet. Praesent gravida mi vitae varius sagittis.</p>
				<a href="#">ver más</a>			
			</li>
		</ul>	
	</div>	
	</section><!--  End participa  -->

	<section class="wrapper">
		<div class="bullet_vertical bullet_vertical_down">
			<img src="/img/bullet_vertical.png">
		</div>
	</section>

	<section class="proyecto wrapper" id="section-about">
		<div class="title animated wow fadeIn">
			<h2>El proyecto</h2>
			<h3>¿Qu&eacute; desarrollo en Uruguay?</h3>
			<hr class="separator"/>
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non tristique nulla. Aenean et vulputate lacus. Pellentesque a laoreet risus, nec convallis est. Phasellus vitae arcu placerat, sagittis orci eu, molestie lectus. Vivamus sed arcu turpis. Suspendisse non cursus metus, eget ullamcorper mauris. Curabitur pellentesque, eros vitae fringilla hendrerit, tellus mauris sagittis odio, ullamcorper eleifend nulla ligula id magna. Aliquam non quam in turpis iaculis venenatis.</p>
		<p>Aenean non sem interdum magna aliquet facilisis. Sed id cursus justo, sit amet scelerisque leo. Nullam accumsan mattis gravida. Cras eu eros eget orci placerat tempus eget eu quam. Ut commodo dui ut ex sollicitudin hendrerit. Nam pellentesque laoreet nisi id interdum. Integer efficitur tempor semper. Nunc tempor efficitur hendrerit. Morbi ultrices velit nibh, in hendrerit orci vestibulum eu.</p>
		<a href="#">ver más</a>		
		<div class="title animated wow fadeIn">
			<hr class="separator" style="margin-top:20px;"/>
		</div>
	</section><!--  End proyecto  -->

	<section class="videos_home">
		<div class="wrapper">
			<div class="videos_home_content">
				<div class="videos_home_content_video"><img src="/img/thumb_video.jpg"></div>
				<section>
					<h2>Aliquam vel dolor sed urna fringilla rhoncus eu sit amet nunc</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non tristique nulla. Aenean et vulputate lacus. Pellentesque a laoreet risus, nec convallis est. Phasellus vitae arcu placerat, sagittis orci eu, molestie lectus. Vivamus sed arcu turpis. Suspendisse non cursus metus, eget ullamcorper mauris. Curabitur pellentesque, eros vitae fringilla hendrerit, tellus mauris sagittis odio, ullamcorper eleifend nulla ligula id magna. Aliquam non quam in turpis iaculis venenatis.
					Aenean non sem interdum magna aliquet facilisis. Sed id cursus justo, sit amet scelerisque leo.</p>
					<a href="#">ver más</a>	
				</section>
			</div>
		</div>
	</section><!--  End videos  -->

	<section class="contacto wrapper" id="section-about">
		<div class="title animated wow fadeIn">
			<h2>contacto</h2>
			<hr class="separator"/>
		</div>
		<form>
			<input type="text" placeholder="NOMBRE*"><input type="text" placeholder="MAIL*">
			<textarea placeholder="MENSAJE*" rows="10"></textarea>
			<span><sub>*</sub>Campos obligatorios</span>
			<input type="submit" class="input_submit" value="enviar" style="margin-left:495px;">
		</form>
		<div class="clear"></div>
		<div class="title animated wow fadeIn">
			<hr class="separator" style="margin-top:20px;"/>
		</div>
	</section><!--  End contacto  -->

	<!-- videos <section class="videos_home">
		<div class="wrapper">
			<div class="title animated wow fadeIn">
				<h2>Recent Posts</h2>
				<h3>the most recent posts from our blog</h3>
				<hr class="separator"/>
			</div>

			<ul class="clearfix">
				<li class="animated wow fadeInDown">
					<div class="media">
						<div class="date">
							<span class="day">25</span>
							<span class="month">Jun</span>
						</div>
						<a href="#">
							<img src="img/blog_post1.jpg" alt=""/>
						</a>
					</div>
					<a href="#">
						<h1>Sed do eiusmod tempor incididunt.</h1>
					</a>
				</li>

				<li class="animated wow fadeInDown" data-wow-delay=".2s">
					<div class="media">
						<div class="date">
							<span class="day">11</span>
							<span class="month">May</span>
						</div>
						<a href="#">
							<img src="img/blog_post2.jpg" alt=""/>
						</a>
					</div>					
					<a href="#">
						<h1>Velit esse cillum dollore fugiat nulla.</h1>
					</a>
				</li>

				<li class="animated wow fadeInDown" data-wow-delay=".4s">
					<div class="media">
						<div class="date">
							<span class="day">13</span>
							<span class="month">Feb</span>
						</div>
						<a href="#">
							<img src="img/blog_post3.jpg" alt=""/>
						</a>
					</div>
					<a href="#">
						<h1>Officia deserunt mollit est anim laborum.</h1>
					</a>
				</li>

				<li class="animated wow fadeInDown" data-wow-delay=".6s">
					<div class="media">
						<div class="date">
							<span class="day">10</span>
							<span class="month">Jan</span>
						</div>
						<a href="#">
							<img src="img/blog_post4.jpg" alt=""/>
						</a>
					</div>
					<a href="#"><h1>Culpa qui officia deserunt 
					mollit ani
					m.</h1>
				</a>
				</li>
			</ul>
		</div>
	</section> End videos  -->


<?php include('_footer.php');?>
    <script src='../ga.js'></script>
</body>
</html>