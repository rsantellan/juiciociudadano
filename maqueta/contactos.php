<!DOCTYPE html>
<?php $categoria  = 'proyecto';?>
<?php $pagina  = 'nosotros';?>
<html lang="en">
<?php include('_head.php');?>
<body>
<?php include('_header.php');?>


	<section class="wrapper">
		<div class="bullet_vertical">
			<img src="/img/bullet_vertical.png">
		</div>
	</section>

	<section class="wrapper internas">
		<div class="internas_content">
			<img src="/img/contactos.png" alt="" class="animated wow fadeInDown"/>
		</div>	
		<div class="title animated wow fadeIn">
		<h1>nuestros contactos</h1>
		</div>
	</section><!--  End participa  -->

	<div class="title animated wow fadeIn">
		<hr class="separator"/>
	</div>

	<section class="internas wrapper" id="section-about">
		<p>María Natalia Rodríguez</br>Coordinadora del componente comunicación del proyecto</br><a href="mailto:comunicacion@quedesarrollo.uy">comunicacion@quedesarrollo.uy</a></br>Cel: (+598) 092 55 66 57</p>
		<div class="title animated wow fadeIn">
			<hr class="separator" style="margin-top:20px;"/>
		</div>
	</section><!--  End proyecto  -->


	<section class="contacto_internas">
		<div class="wrapper">
			<div class="contacto_internas_content">
		<div class="title animated wow fadeIn">
			<h2>contacto</h2>
		</div>
		<form>
			<input type="text" placeholder="NOMBRE*"><input type="text" placeholder="MAIL*">
			<textarea placeholder="MENSAJE*" rows="10"></textarea>
			<span><sub>*</sub>Campos obligatorios</span>
			<input type="submit" class="input_submit" value="enviar" style="margin-left:495px;">
		</form>
		<div class="clear"></div>
	</section><!--  End contacto  -->


<?php include('_footer.php');?>
    <script src='../ga.js'></script>
</body>
</html>