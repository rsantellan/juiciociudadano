<!DOCTYPE html>
<?php $categoria  = 'biblioteca';?>
<?php $pagina  = 'textos';?>
<html lang="en">
<?php include('_head.php');?>
<body>
<?php include('_header.php');?>


	<section class="wrapper">
		<div class="bullet_vertical">
			<img src="/img/bullet_vertical.png">
		</div>
	</section>

	<section class="wrapper internas">
		<div class="internas_content">
			<img src="/img/biblioteca.png" alt="" class="animated wow fadeInDown"/>
		</div>	
		<div class="title animated wow fadeIn">
		<h1>textos</h1>
		</div>
	</section><!--  End participa  -->

	<section class="wrapper">
		<div class="bullet_vertical bullet_vertical_down">
			<img src="/img/bullet_vertical.png">
		</div>
	</section>

	<section class="internas wrapper" id="section-about">
		<div class="title animated wow fadeIn">
			<h2>texto 1</h2>
			<hr class="separator"/>
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel orci consequat, fermentum mauris luctus, semper mauris. Vivamus quis elit orci. Ut massa eros, malesuada at tellus eu, tincidunt tincidunt urna. Nam malesuada sem porta tortor egestas, id posuere lorem consectetur. Nam vel quam ut augue convallis porttitor. Sed malesuada, mi ac lacinia ullamcorper, elit sapien rhoncus augue, pulvinar placerat eros risus quis mauris. Cras sagittis bibendum dignissim. In hac habitasse platea dictumst. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas efficitur posuere purus, at ultrices neque vulputate ut. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam et purus non felis eleifend vestibulum a vitae nulla. Duis pellentesque magna eu ligula convallis rutrum. Nullam erat lacus, commodo at metus ut, scelerisque egestas tortor. Ut nec feugiat leo. Suspendisse laoreet metus sit amet nibh varius mattis.</p>
		<p>Donec tempor luctus ultrices. Praesent pretium consectetur ex eget fermentum. Duis efficitur nisi in feugiat cursus. Donec id tincidunt urna. Ut quam enim, sodales id urna cursus, cursus porta nisi. Nunc elementum risus urna, id tempor libero porta eu. Nunc a massa sodales magna rutrum dapibus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam at finibus ligula. Fusce feugiat consectetur nisi eu rhoncus.</p>
		<p>Sed interdum molestie commodo. Nulla et feugiat felis. Curabitur eu felis arcu. Duis ultricies lectus non turpis viverra consectetur. Quisque iaculis vel ipsum ac semper. Duis malesuada porta enim quis malesuada. Suspendisse non nisl at eros gravida molestie. Aliquam ut dictum urna. Aliquam erat volutpat. Donec ac libero in libero vestibulum hendrerit nec id odio.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel orci consequat, fermentum mauris luctus, semper mauris. Vivamus quis elit orci. Ut massa eros, malesuada at tellus eu, tincidunt tincidunt urna. Nam malesuada sem porta tortor egestas, id posuere lorem consectetur. Nam vel quam ut augue convallis porttitor. Sed malesuada, mi ac lacinia ullamcorper, elit sapien rhoncus augue, pulvinar placerat eros risus quis mauris. Cras sagittis bibendum dignissim. In hac habitasse platea dictumst. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas efficitur posuere purus, at ultrices neque vulputate ut. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam et purus non felis eleifend vestibulum a vitae nulla. Duis pellentesque magna eu ligula convallis rutrum. Nullam erat lacus, commodo at metus ut, scelerisque egestas tortor. Ut nec feugiat leo. Suspendisse laoreet metus sit amet nibh varius mattis.</p>
		<p>Donec tempor luctus ultrices. Praesent pretium consectetur ex eget fermentum. Duis efficitur nisi in feugiat cursus. Donec id tincidunt urna. Ut quam enim, sodales id urna cursus, cursus porta nisi. Nunc elementum risus urna, id tempor libero porta eu. Nunc a massa sodales magna rutrum dapibus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam at finibus ligula. Fusce feugiat consectetur nisi eu rhoncus.</p>		
		<img src="/img/resultados1.jpg">
		<img src="/img/resultados2.jpg">		
		<div class="title animated wow fadeIn">
			<hr class="separator" style="margin-top:20px;"/>
		</div>
	</section><!--  End textos  -->



	<section class="contacto_internas">
		<div class="wrapper">
			<div class="contacto_internas_content">
		<div class="title animated wow fadeIn">
			<h2>contacto</h2>
		</div>
		<form>
			<input type="text" placeholder="NOMBRE*"><input type="text" placeholder="MAIL*">
			<textarea placeholder="MENSAJE*" rows="10"></textarea>
			<span><sub>*</sub>Campos obligatorios</span>
			<input type="submit" class="input_submit" value="enviar" style="margin-left:495px;">
		</form>
		<div class="clear"></div>
	</section><!--  End contacto  -->


<?php include('_footer.php');?>
    <script src='../ga.js'></script>
</body>
</html>